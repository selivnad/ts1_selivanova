package cz.cvut.fel.ts1;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class CalculatorTest {
    private Calculator calculator = new Calculator();

    @Test
    public void testAdd() {
        Assertions.assertEquals(5, calculator.add(2, 3), "Addition test failed");
    }

    @Test
    public void testSubtract() {
        Assertions.assertEquals(1, calculator.subtract(3, 2), "Subtraction test failed");
    }

    @Test
    public void testMultiply() {
        Assertions.assertEquals(6, calculator.multiply(2, 3), "Multiplication test failed");
    }

    @Test
    public void testDivide() {
        Assertions.assertEquals(2, calculator.divide(6, 3), "Division test failed");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivideByZero() {
        calculator.divide(1, 0);
    }
}
